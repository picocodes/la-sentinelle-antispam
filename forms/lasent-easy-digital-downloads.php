<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}



/*
 * Disable AJAX for Easy Digital Downloads.
 *
 * @since 1.8.0
 *
 * @uses "edd_is_ajax_disabled" filter.
 * @uses "la_sentinelle-edd-disable-ajax" option.
 *
 * @param  bool True when EDD AJAX is disabled (for the cart), false otherwise.
 * @return bool True when EDD AJAX is disabled (for the cart), false otherwise.
 *
 */
function la_sentinelle_edd_is_ajax_disabled( $bool ) {

	if ( get_option( 'la_sentinelle-edd-disable-ajax', false ) === 'true' ) {
		return true; // AJAX is disabled.
	}

	return $bool;

}
add_filter( 'edd_is_ajax_disabled', 'la_sentinelle_edd_is_ajax_disabled', 99, 1 );
