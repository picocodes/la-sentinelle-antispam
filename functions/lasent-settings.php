<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * Register Settings.
 *
 * @since 1.0.0
 */
function la_sentinelle_register_settings() {
	register_setting( 'la_sentinelle_options', 'la_sentinelle-caldera_blocked',    'intval' ); // int
	register_setting( 'la_sentinelle_options', 'la_sentinelle-cf7_blocked',        'intval' ); // int
	register_setting( 'la_sentinelle_options', 'la_sentinelle-edd-disable-ajax',   'strval' ); // 'false'
	register_setting( 'la_sentinelle_options', 'la_sentinelle-everest',            'strval' ); // 'true'
	register_setting( 'la_sentinelle_options', 'la_sentinelle-everest_blocked',    'intval' ); // int
	register_setting( 'la_sentinelle_options', 'la_sentinelle-formidable',         'strval' ); // 'true'
	register_setting( 'la_sentinelle_options', 'la_sentinelle-formidable_blocked', 'intval' ); // int
	register_setting( 'la_sentinelle_options', 'la_sentinelle-honeypot',           'strval' ); // 'true'
	register_setting( 'la_sentinelle_options', 'la_sentinelle-honeypot_value',     'intval' ); // random 1 - 100
	register_setting( 'la_sentinelle_options', 'la_sentinelle-nonce',              'strval' ); // 'false'
	register_setting( 'la_sentinelle_options', 'la_sentinelle-noptin',             'strval' ); // 'true'
	register_setting( 'la_sentinelle_options', 'la_sentinelle-noptin_blocked',     'strval' ); // int
	register_setting( 'la_sentinelle_options', 'la_sentinelle-remove_comments',    'strval' ); // 'false'
	register_setting( 'la_sentinelle_options', 'la_sentinelle-save_comments',      'strval' ); // 'true'
	register_setting( 'la_sentinelle_options', 'la_sentinelle-sfs',                'strval' ); // 'true'
	register_setting( 'la_sentinelle_options', 'la_sentinelle-timeout',            'strval' ); // 'true'
	register_setting( 'la_sentinelle_options', 'la_sentinelle-version',            'strval' ); // string
	register_setting( 'la_sentinelle_options', 'la_sentinelle-wpcomment',          'strval' ); // string
	register_setting( 'la_sentinelle_options', 'la_sentinelle-wpcomments_blocked', 'intval' ); // int
	register_setting( 'la_sentinelle_options', 'la_sentinelle-wplogin',            'strval' ); // string
	register_setting( 'la_sentinelle_options', 'la_sentinelle-wplogin_blocked',    'intval' ); // int
	register_setting( 'la_sentinelle_options', 'la_sentinelle-wppassword',         'strval' ); // string
	register_setting( 'la_sentinelle_options', 'la_sentinelle-wppassword_blocked', 'intval' ); // int
	register_setting( 'la_sentinelle_options', 'la_sentinelle-wpregister',         'strval' ); // string
	register_setting( 'la_sentinelle_options', 'la_sentinelle-wpregister_blocked', 'intval' ); // int
}
add_action( 'admin_init', 'la_sentinelle_register_settings' );


/*
 * Set default options.
 * Idea is to have all options in the database and thus cached, so we hit an empty cache less often.
 *
 * @since 1.0.0
 */
function la_sentinelle_set_defaults() {

	if ( get_option('la_sentinelle-edd-disable-ajax', false) === false ) {
		update_option( 'la_sentinelle-edd-disable-ajax', 'false' );
	}
	if ( get_option('la_sentinelle-everest', false) === false ) {
		update_option( 'la_sentinelle-everest', 'true' );
	}
	if ( get_option('la_sentinelle-formidable', false) === false ) {
		update_option( 'la_sentinelle-formidable', 'true' );
	}
	if ( get_option('la_sentinelle-honeypot', false) === false ) {
		update_option( 'la_sentinelle-honeypot', 'true' );
	}
	if ( get_option('la_sentinelle-honeypot_value', false) === false ) {
		$random = rand( 1, 99 );
		update_option( 'la_sentinelle-honeypot_value', $random );
	}
	if ( get_option('la_sentinelle-nonce', false) === false ) {
		update_option( 'la_sentinelle-nonce', 'false' );
	}
	if ( get_option('la_sentinelle-noptin', false) === false ) {
		update_option( 'la_sentinelle-noptin', 'true' );
	}
	if ( get_option('la_sentinelle-remove_comments', false) === false ) {
		update_option( 'la_sentinelle-remove_comments', 'false' );
	}
	if ( get_option('la_sentinelle-save_comments', false) === false ) {
		update_option( 'la_sentinelle-save_comments', 'true' );
	}
	if ( get_option('la_sentinelle-sfs', false) === false ) {
		update_option( 'la_sentinelle-sfs', 'false' );
	}
	if ( get_option('la_sentinelle-timeout', false) === false ) {
		update_option( 'la_sentinelle-timeout', 'true' );
	}
	if ( get_option('la_sentinelle-wpcomment', false) === false ) {
		update_option( 'la_sentinelle-wpcomment', 'true' );
	}
	if ( get_option('la_sentinelle-wplogin', false) === false ) {
		update_option( 'la_sentinelle-wplogin', 'true' );
	}
	if ( get_option('la_sentinelle-wppassword', false) === false ) {
		update_option( 'la_sentinelle-wppassword', 'true' );
	}
	if ( get_option('la_sentinelle-wpregister', false) === false ) {
		update_option( 'la_sentinelle-wpregister', 'true' );
	}

	update_option('la_sentinelle-version', LASENT_VER);

}
