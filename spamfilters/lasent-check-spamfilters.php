<?php

// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * Check form fields for a form.
 *
 * @return string result 'spam' if it is considered spam.
 *
 * @since 1.0.0
 */
function la_sentinelle_check_spamfilters( $nonce_action = 'default' ) {

	$marker = la_sentinelle_check_nonce( $nonce_action );
	if ( $marker === 'spam' ) {
		return 'spam';
	}
	$marker = la_sentinelle_check_honeypot();
	if ( $marker === 'spam' ) {
		return 'spam';
	}
	$marker = la_sentinelle_check_timeout();
	if ( $marker === 'spam' ) {
		return 'spam';
	}

	return '';

}
