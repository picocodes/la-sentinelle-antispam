<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * Register post type for logging.
 *
 * @since 2.0.0
 */
function la_sentinelle_register_log_post_type() {

	register_post_type( 'la_sentinelle_log', array(
			'labels'          => array(
				'name'          => esc_html__( 'La Sentinelle Logs', 'la-sentinelle-antispam' ),
				'singular_name' => esc_html__( 'Submission Log', 'la-sentinelle-antispam' ),
			),
			'rewrite'         => false,
			'query_var'       => false,
			'public'          => false,
			'supports'        => array( 'editor', 'custom-fields' ),
			'capability_type' => 'page',
		)
	);

}
add_action( 'init', 'la_sentinelle_register_log_post_type' );


/*
 * Save spam submission of plugin forms in the database in its own custom post type.
 *
 * @param $plugin_slug string Slug of the plugin that provided the form of this submission.
 * @param $spamfilters mixed  Array or string with list of spamfilters that were triggered and recognized the submisson as spam.
 *
 * @return $post_id    int Number higher than 0 if it was saved in the database.
 *
 * @since 2.0.0
 */
function la_sentinelle_save_spam_submission( $plugin_slug, $spamfilters ) {

	if (get_option( 'la_sentinelle-save_comments', 'true') !== 'true') {
		return 0;
	}

	/* Setting both dates will set the published date to this. */
	$post_date = current_time( 'mysql' );
	$post_date_gmt = get_gmt_from_date( $post_date );

	$post_content = '';
	$postdata = $_POST;
	foreach ( $postdata as $key => $value ) {
		if ( is_array( $value ) && ! empty( $value ) ) {
			//$value = implode( ', ', $value );
			$value = print_r( $value, true ); // take also care of arrays in arrays.
		}
		$post_content .= $key . ': ' . $value . "\n";
	}
	$post_content = wp_kses( $post_content, '' );

	$plugin_slug = sanitize_text_field( $plugin_slug );

	if ( is_array( $spamfilters ) && ! empty( $spamfilters ) ) {
		$spamfilters = sanitize_text_field( implode( ', ', $spamfilters ) );
	} else if ( is_string( $spamfilters ) && strlen( $spamfilters ) > 0 ) {
		$spamfilters = sanitize_text_field( $spamfilters );
	} else {
		$spamfilters = '';
	}

	$post_data = array(
		'post_parent'    => 0,
		'post_status'    => 'draft',
		'post_type'      => 'la_sentinelle_log',
		'post_date'      => $post_date,
		'post_date_gmt'  => $post_date_gmt,
		'post_author'    => get_current_user_id(),
		'post_password'  => '',
		'post_content'   => $post_content,
		'menu_order'     => 0,
	);
	$post_id = wp_insert_post( $post_data );

	/* Bail if no post was added. */
	if ( empty( $post_id ) ) {
		return 0;
	}

	$post_meta = array(
		'lasent_plugin_slug' => $plugin_slug,
		'lasent_spamfilters' => $spamfilters,
	);

	// Insert post meta.
	foreach ( $post_meta as $meta_key => $meta_value ) {
		update_post_meta( $post_id, $meta_key, $meta_value );
	}

	do_action( 'la_sentinelle_after_save_spam_submission', $post_id );

	return $post_id;

}
