<?php

// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * Get honeypot fields for a form.
 *
 * @param  bool   first field or second field.
 * @return string html with input fields.
 *
 * @since 1.0.0
 */
function la_sentinelle_get_honeypot( $first_field ) {

	$output = '';

	if ( get_option( 'la_sentinelle-honeypot', 'true') === 'true' ) {
		if (  $first_field ) {
			$field_name = la_sentinelle_get_field_name( 'honeypot' );
			$honeypot_value = get_option( 'la_sentinelle-honeypot_value', 15 );
			$output .= '
			<input value="' . $honeypot_value . '" type="hidden" name="' . $field_name . '" class="' . $field_name . '" placeholder="" />
			';
		} else {
			$field_name2 = la_sentinelle_get_field_name( 'honeypot2' );
			$output .= '
			<input value="" type="hidden" name="' . $field_name2 . '" class="' . $field_name2 . '" placeholder="" />
			';
		}
	}

	return $output;

}


/*
 * Check honeypot fields for a form.
 *
 * @return string result 'spam' if it is considered spam.
 *
 * @since 1.0.0
 */
function la_sentinelle_check_honeypot() {

	$post_data = $_POST;
	$marked_by_honeypot = false;

	if (get_option( 'la_sentinelle-honeypot', 'true') === 'true') {
		if ( is_array( $post_data ) && ! empty( $post_data ) ) {
			$field_name = la_sentinelle_get_field_name( 'honeypot' );
			$field_name2 = la_sentinelle_get_field_name( 'honeypot2' );
			$honeypot_value = get_option( 'la_sentinelle-honeypot_value', 15 );
			if ( isset($_POST["$field_name"]) && strlen($_POST["$field_name"]) > 0 ) {
				// Input field was filled in, so considered spam.
				$marked_by_honeypot = true;
			}
			if ( ! isset($_POST["$field_name2"]) || $_POST["$field_name2"] !== $honeypot_value ) {
				// Input field was not filled in correctly, so considered spam.
				$marked_by_honeypot = true;
			}
		}
	}

	if ( $marked_by_honeypot ) {
		return 'spam';
	}

	return '';

}
