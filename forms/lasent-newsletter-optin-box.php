<?php


// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}



/*
 * Add field in Newsletter Optin Box to the shortcode form and new widget form.
 * do_action( 'after_print_noptin_form_fields', $singleLine, $id );
 *
 * @since 2.0.0
 *
 * @uses "after_print_noptin_form_fields" action
 * @uses "before_noptin_quick_widget_submit" action
 *
 * @return string html with the input fields.
 *
 */
function la_sentinelle_after_print_noptin_form_fields() {

	echo la_sentinelle_get_spamfilters();

}
if (get_option( 'la_sentinelle-noptin', 'true') === 'true') {
	add_action( 'after_print_noptin_form_fields', 'la_sentinelle_after_print_noptin_form_fields', 10, 0 ); // shortcode, popup, slide_in, form widget
	add_action( 'before_noptin_quick_widget_submit', 'la_sentinelle_after_print_noptin_form_fields', 10, 0 ); // new form widget
}


/*
 * Validate shortcode form in Newsletter Optin Box
 *
 * @since 2.0.0
 *
 * @uses "noptin_before_add_ajax_subscriber" action
 *
 */
function la_sentinelle_noptin_before_add_ajax_subscriber() {

	$spamfilters = array();
	$errors = '';
	$marker_nonce = la_sentinelle_check_nonce();
	if ( $marker_nonce === 'spam' ) {
		$spamfilters[] = 'nonce';
		$errors .= esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) . ' ';
	}
	$marker_honeypot = la_sentinelle_check_honeypot();
	if ( $marker_honeypot === 'spam' ) {
		$spamfilters[] = 'honeypot';
		$errors .= esc_html__( 'Your submission was marked as spam, please try again or contact a site administrator for assistance.', 'la-sentinelle-antispam' ) . ' ';
	}
	$marker_timeout = la_sentinelle_check_timeout();
	if ( $marker_timeout === 'spam' ) {
		$spamfilters[] = 'timeout';
		$errors .= esc_html__( 'Your submission was sent in too fast. Please slow down and try again.', 'la-sentinelle-antispam' ) . ' ';
	}

	if ( $marker_nonce === 'spam' || $marker_honeypot === 'spam' || $marker_timeout === 'spam' ) {
		la_sentinelle_add_statistic_blocked( 'noptin' );
		la_sentinelle_save_spam_submission( 'newsletter-optin-box', $spamfilters );
		wp_die( $errors );
	}

}
if (get_option( 'la_sentinelle-noptin', 'true') === 'true') {
	add_action( 'noptin_before_add_ajax_subscriber', 'la_sentinelle_noptin_before_add_ajax_subscriber' );
}
