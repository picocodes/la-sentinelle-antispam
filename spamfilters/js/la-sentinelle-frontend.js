/*
 * JavaScript for La Sentinelle antispam.
 *

Copyright 2018 - 2021  Marcel Pol  (email: marcel@timelord.nl)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


/*
 * Mangle data for the honeypot.
 *
 * @since 1.0.0
 */
jQuery(document).ready(function($) {
	jQuery( 'form' ).each( function( index, form ) {

		var honeypot  = la_sentinelle_frontend_script.honeypot;
		var honeypot2 = la_sentinelle_frontend_script.honeypot2;

		var honeypot_val = parseInt( jQuery( 'input.' + honeypot, form ).val(), 10 );
		var honeypot2_val = parseInt( jQuery( 'input.' + honeypot2, form ).val(), 10 );

		if ( ! isNaN( honeypot_val ) && (typeof honeypot_val != "undefined") && (typeof honeypot2_val != "undefined") ) {
			la_sentinelle_honeypot( form );
		}

	});

	// Hook into this.reset for Contact Form 7.
	jQuery('form.wpcf7-form').on('reset', function() {

		var form = this;
		setTimeout(function() {
				la_sentinelle_honeypot( form );
			}, 500 );

	});
});
function la_sentinelle_honeypot( form ) {

	var honeypot  = la_sentinelle_frontend_script.honeypot;
	var honeypot2 = la_sentinelle_frontend_script.honeypot2;

	var honeypot_val = parseInt( jQuery( 'input.' + honeypot, form ).val(), 10 );
	var honeypot2_val = parseInt( jQuery( 'input.' + honeypot2, form ).val(), 10 );

	if ( ! isNaN( honeypot_val ) && (typeof honeypot_val != "undefined") && (typeof honeypot2_val != "undefined") ) {
		if ( honeypot_val > 0 ) {
			jQuery( 'input.' + honeypot2, form ).val( honeypot_val );
			jQuery( 'input.' + honeypot, form ).val( '' );
		}
	}
}


/*
 * Mangle data for the form timeout.
 *
 * @since 1.0.0
 */
jQuery(document).ready(function($) {
	jQuery( 'form' ).each( function( index, form ) {

		var timeout  = la_sentinelle_frontend_script.timeout;
		var timeout2 = la_sentinelle_frontend_script.timeout2;

		var timer  = parseInt( jQuery( 'input.' + timeout, form ).val(), 10 );
		var timer2 = parseInt( jQuery( 'input.' + timeout2, form ).val(), 10 );

		if ( ! isNaN( timer ) && ! isNaN( timer2 ) &&(typeof timer != "undefined") && (typeof timer2 != "undefined") ) {

			var form = this;

			var timeout  = la_sentinelle_frontend_script.timeout;
			var timeout2 = la_sentinelle_frontend_script.timeout2;

			var timer  = parseInt( jQuery( 'input.' + timeout, form ).val(), 10 );
			var timer2 = parseInt( jQuery( 'input.' + timeout2, form ).val(), 10 );

			var timer  = timer - 1
			var timer2 = timer2 + 1

			jQuery( 'input.' + timeout, form ).val( timer );
			jQuery( 'input.' + timeout2, form ).val( timer2 );

		}
	});
});
