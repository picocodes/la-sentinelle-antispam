<?php

// No direct calls to this script
if ( strpos($_SERVER['PHP_SELF'], basename(__FILE__) )) {
	die('No direct calls allowed!');
}


/*
 * Get form timeout fields for a form.
 *
 * @return string html with input fields.
 *
 * @since 1.0.0
 */
function la_sentinelle_get_timeout() {

	$output = '';

	if ( get_option( 'la_sentinelle-timeout', 'true') === 'true' ) {
		$field_name = la_sentinelle_get_field_name( 'timeout' );
		$field_name2 = la_sentinelle_get_field_name( 'timeout2' );
		$random = rand( 100, 100000 );
		$output .= '
			<input value="' . $random . '" type="hidden" name="' . $field_name . '" class="' . $field_name . '" placeholder="" />
			<input value="' . $random . '" type="hidden" name="' . $field_name2 . '" class="' . $field_name2 . '" placeholder="" />
			';
	}

	return $output;

}


/*
 * Check timeout fields for a form.
 *
 * @return string result 'spam' if it is considered spam.
 *
 * @since 1.0.0
 */
function la_sentinelle_check_timeout() {

	$post_data = $_POST;
	$marked_by_timeout = false;

	if (get_option( 'la_sentinelle-timeout', 'true') === 'true') {
		if ( is_array( $post_data ) && ! empty( $post_data ) ) {
			$field_name = la_sentinelle_get_field_name( 'timeout' );
			$field_name2 = la_sentinelle_get_field_name( 'timeout2' );
			if ( isset($_POST["$field_name"]) && strlen($_POST["$field_name"]) > 0 && isset($_POST["$field_name2"]) && strlen($_POST["$field_name2"]) > 0 ) {
				// Input fields were filled in, so continue.
				$timeout  = (int) $_POST["$field_name"];
				$timeout2 = (int) $_POST["$field_name2"];
				if ( ( $timeout2 - $timeout ) < 2 ) {
					// Submitted less then 1 seconds after loading. Considered spam.
					$marked_by_timeout = true;
				}
			} else {
				// Input fields were not filled in correctly. Considered spam.
				$marked_by_timeout = true;
			}
		}
	}

	if ( $marked_by_timeout ) {
		return 'spam';
	}

	return '';

}
